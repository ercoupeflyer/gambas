# Catalan translation of Object
# Copyright (C) 2000-2010 Benoît Minisini.
# This file is distributed under the same license as the Object package.
# Jordi Sayol <g.sayol@yahoo.es>, 2007-2010.
#
#
msgid ""
msgstr ""
"Project-Id-Version: Object\n"
"POT-Creation-Date: 2002-11-01 04:27+0100\n"
"PO-Revision-Date: 2010-12-16 23:31+0100\n"
"Last-Translator: Jordi Sayol <g.sayol@yahoo.es>\n"
"Language-Team: \n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-Language: Catalan\n"

#: .project:1
msgid "Object manipulation example"
msgstr "Exemple de manipulacicó d'objectes"

#: FStart.form:12
msgid "Object "
msgstr "Objecte"

#: FStart.form:17
msgid "create the Thing !"
msgstr "crea l'objecte!"

#: FStart.form:22
msgid "check the Thing !"
msgstr "verifica l'objecte!"

#: FStart.form:33
msgid "destroy the Thing !"
msgstr "destrueix l'objecte!"

#: FStart.form:38
msgid " by juergen@zdero.com"
msgstr "per juergen@zdero.com"

# Catalan translation of gb.form
# Copyright (C) 2000-2010 Benoît Minisini.
# This file is distributed under the same license as gb.form package.
# Jordi Sayol <g.sayol@yahoo.es>, 2007-2010.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: gb.form 3.15.90\n"
"POT-Creation-Date: 2021-02-17 15:39 UTC\n"
"PO-Revision-Date: 2021-02-17 15:40 UTC\n"
"Last-Translator: Benoît Minisini <g4mba5@gmail.com>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "More controls for graphical components"
msgstr "Més controls per als components gràfics"

#: CBookmarkList.class:51
msgid "Home"
msgstr "Inici"

#: CBookmarkList.class:53
msgid "Desktop"
msgstr "Escriptori"

#: CBookmarkList.class:55
msgid "System"
msgstr "Sistema"

#: ColorPalette.class:143
msgid "Last colors"
msgstr ""

#: ColorPalette.class:244
msgid "Remove color"
msgstr ""

#: ColorPalette.class:248
msgid "Remove all colors"
msgstr ""

#: ColorPalette.class:252
msgid "Sort colors"
msgstr ""

#: DirView.class:535
msgid "Cannot rename directory."
msgstr "No es pot canviar el nom del directori."

#: DirView.class:566
msgid "New folder"
msgstr "Nova carpeta"

#: DirView.class:585
msgid "Cannot create directory."
msgstr "No es pot crear el directori."

#: FBugFileView.form:32
msgid "Toggle Fileview Detailed View"
msgstr ""

#: FBugFileView.form:37
msgid "Toggle Filechooser Detailed View"
msgstr ""

#: FCalendar.form:48
msgid "Today"
msgstr "Avui"

#: FCalendar.form:54
msgid "Previous month"
msgstr "Mes anterior"

#: FCalendar.form:60
msgid "Next month"
msgstr "Mes següent"

#: FCalendar.form:147
msgid "Apply"
msgstr ""

#: FColorChooser.form:81
msgid "Follow color grid"
msgstr "Seguiu la graella de color"

#: FDirChooser.class:438
msgid "Directory not found."
msgstr ""

#: FDirChooser.class:547
msgid "All files (*)"
msgstr "Tots els fitxers (*)"

#: FDirChooser.class:886
msgid "&Overwrite"
msgstr "&Sobreescriu"

#: FDirChooser.class:886
msgid ""
"This file already exists.\n"
"\n"
"Do you want to overwrite it?"
msgstr ""
"Aquest fitxer ja existeix.\n"
"\n"
"El voleu sobreescriure?"

#: FDirChooser.class:1004
msgid "&Bookmark current directory"
msgstr "Crea adreça d'&interès del directori actual"

#: FDirChooser.class:1012
msgid "&Edit bookmarks..."
msgstr "&Edita les adreces d'interès..."

#: FDirChooser.class:1023
msgid "Show &details"
msgstr "Mostra els &detalls"

#: FDirChooser.class:1029
msgid "Show &hidden files"
msgstr "Mostra els fitxers &ocults"

#: FDirChooser.class:1035
msgid "Show &image preview"
msgstr ""

#: FDirChooser.class:1043
msgid "&Rename"
msgstr ""

#: FDirChooser.class:1048
msgid "Copy"
msgstr "Copia"

#: FDirChooser.class:1053
msgid "&Delete"
msgstr ""

#: FDirChooser.class:1065
msgid "&Uncompress file"
msgstr "&Descomprimeix el fitxer"

#: FDirChooser.class:1070
msgid "&Create directory"
msgstr "&Crea un directori"

#: FDirChooser.class:1075
msgid "Open in &file manager..."
msgstr "Obre amb el gestor de &fitxers..."

#: FDirChooser.class:1080
msgid "&Refresh"
msgstr "&Actualitza"

#: FDirChooser.class:1088
msgid "&Properties"
msgstr ""

#: FDirChooser.class:1302
msgid "Overwrite"
msgstr "Sobreescriure"

#: FDirChooser.class:1302
msgid "Overwrite all"
msgstr "Sobreescriure-ho tot"

#: FDirChooser.class:1302
msgid "This file or directory already exists."
msgstr "Aquest fitxer o directori ja existeix."

#: FDirChooser.class:1323
msgid "Cannot list archive contents"
msgstr ""

#: FDirChooser.class:1363
msgid "Cannot uncompress file."
msgstr "No es pot descomprimir el fitxer."

#: FDirChooser.class:1363
msgid "Unknown archive."
msgstr "Fitxer desconegut."

#: FDirChooser.class:1431
msgid "Delete file"
msgstr ""

#: FDirChooser.class:1432
msgid "Do you really want to delete that file?"
msgstr ""

#: FDirChooser.class:1439
msgid "Unable to delete file."
msgstr ""

#: FDirChooser.class:1449
msgid "Delete directory"
msgstr ""

#: FDirChooser.class:1450
msgid "Do you really want to delete that directory?"
msgstr ""

#: FDirChooser.class:1457
msgid "Unable to delete directory."
msgstr ""

#: FDirChooser.form:68
msgid "Parent directory"
msgstr ""

#: FDirChooser.form:74
msgid "Root directory"
msgstr ""

#: FDirChooser.form:139
msgid "Image preview"
msgstr ""

#: FDirChooser.form:147
msgid "Detailed view"
msgstr ""

#: FDirChooser.form:155
msgid "File properties"
msgstr ""

#: FDirChooser.form:161
msgid "Show files"
msgstr "Mostra els fitxers"

#: FDirChooser.form:181
msgid "Bookmarks"
msgstr "Adreces d'interès"

#: FDirChooser.form:248 FInputBox.form:45 FWizard.class:76 Form1.form:36
msgid "OK"
msgstr "D'acord"

#: FDirChooser.form:254 FEditBookmark.class:119 FInputBox.form:51
#: FSidePanel.class:1149 FWizard.form:52 Form1.form:42
msgid "Cancel"
msgstr "Canceŀla"

#: FDocumentView.form:51
msgid "Zoom :"
msgstr ""

#: FDocumentView.form:56
msgid "Show Shadow"
msgstr ""

#: FDocumentView.form:66 FTestTabPanel.form:64
msgid "Padding"
msgstr ""

#: FDocumentView.form:71
msgid "Spacing"
msgstr ""

#: FDocumentView.form:80
msgid "Scale Mode"
msgstr ""

#: FDocumentView.form:89
msgid "Goto :"
msgstr ""

#: FDocumentView.form:95
msgid "Column"
msgstr ""

#: FDocumentView.form:95
msgid "Fill"
msgstr ""

#: FDocumentView.form:95
msgid "Horizontal"
msgstr ""

#: FDocumentView.form:95
msgid "None"
msgstr ""

#: FDocumentView.form:95
msgid "Row"
msgstr ""

#: FDocumentView.form:95
msgid "Vertical"
msgstr ""

#: FDocumentView.form:96 FMain.form:69
msgid "ComboBox1"
msgstr ""

#: FDocumentView.form:101 FMain.form:85 FTestBalloon.form:18
#: FTestCompletion.form:23 FTestMenuButton.form:148 FTestMessageView.form:26
#: FTestWizard.form:24
msgid "Button1"
msgstr ""

#: FDocumentView.form:110
msgid "Columns"
msgstr ""

#: FDocumentView.form:120
msgid "Autocenter"
msgstr ""

#: FEditBookmark.class:23 FileView.class:143
msgid "Name"
msgstr "Nom"

#: FEditBookmark.class:24
msgid "Path"
msgstr "Camí"

#: FEditBookmark.class:119
msgid "Do you really want to remove this bookmark?"
msgstr "Segur que voleu esborrar aquest adreça d'interès?"

#: FEditBookmark.form:15
msgid "Edit bookmarks"
msgstr "Edita les adreces d'interès"

#: FEditBookmark.form:34
msgid "Up"
msgstr "Puja"

#: FEditBookmark.form:40
msgid "Down"
msgstr "Avall"

#: FEditBookmark.form:46 FListEditor.class:265
msgid "Remove"
msgstr "Suprimeix"

#: FEditBookmark.form:57 FFileProperties.form:270 MessageView.class:53
msgid "Close"
msgstr "Tanca"

#: FFileProperties.class:117
msgid "Image"
msgstr "Imatge"

#: FFileProperties.class:122
msgid "Audio"
msgstr ""

#: FFileProperties.class:126
msgid "Video"
msgstr "Vídeo"

#: FFileProperties.class:185
msgid "&1 properties"
msgstr ""

#: FFileProperties.class:216 Main.module:48
msgid "&1 B"
msgstr ""

#: FFileProperties.class:221
msgid "no file"
msgstr ""

#: FFileProperties.class:223
msgid "one file"
msgstr ""

#: FFileProperties.class:225
msgid "files"
msgstr ""

#: FFileProperties.class:229
msgid "no directory"
msgstr ""

#: FFileProperties.class:231
msgid "one directory"
msgstr ""

#: FFileProperties.class:233
msgid "directories"
msgstr ""

#: FFileProperties.form:52
msgid "General"
msgstr "General"

#: FFileProperties.form:81
msgid "Type"
msgstr "Tipus"

#: FFileProperties.form:94
msgid "Link"
msgstr ""

#: FFileProperties.form:108
msgid "Directory"
msgstr "Directori"

#: FFileProperties.form:120 FileView.class:145
msgid "Size"
msgstr "Mida"

#: FFileProperties.form:132 FileView.class:147
msgid "Last modified"
msgstr "Última modificació"

#: FFileProperties.form:144
msgid "Permissions"
msgstr ""

#: FFileProperties.form:157
msgid "Owner"
msgstr ""

#: FFileProperties.form:169
msgid "Group"
msgstr ""

#: FFileProperties.form:180
msgid "Preview"
msgstr "Vista prèvia"

# gb-ignore
#: FFileProperties.form:245
msgid "Errors"
msgstr ""

#: FFontChooser.class:387
msgid "How quickly daft jumping zebras vex"
msgstr "Jove xef, porti whisky amb quinze glaçons d'hidrogen, coi!"

#: FFontChooser.form:95
msgid "Bold"
msgstr "Negreta"

#: FFontChooser.form:102
msgid "Italic"
msgstr "Cursiva"

#: FFontChooser.form:109
msgid "Underline"
msgstr "Subratllat"

#: FFontChooser.form:116
msgid "Strikeout"
msgstr "Barrat"

#: FFontChooser.form:125
msgid "Relative"
msgstr ""

#: FIconPanel.form:18
msgid "Item 0"
msgstr ""

#: FIconPanel.form:23
msgid "Toto"
msgstr ""

#: FIconPanel.form:26
msgid "Item 1"
msgstr ""

#: FIconPanel.form:28
msgid "Item 2"
msgstr ""

#: FIconPanel.form:35
msgid "Item 3"
msgstr ""

#: FLCDLabel.form:15
msgid "12:34"
msgstr ""

#: FListEditor.class:264
msgid "Add"
msgstr "Afegeix"

#: FListEditor.form:45
msgid "Add item"
msgstr ""

#: FListEditor.form:52
msgid "Remove item"
msgstr ""

#: FListEditor.form:59
msgid "Move item up"
msgstr ""

#: FListEditor.form:66
msgid "Move item down"
msgstr ""

#: FMain.class:26
#, fuzzy
msgid "PDF files"
msgstr "Mostra els fitxers"

#: FMain.class:26
msgid "Postscript files"
msgstr ""

#: FMain.form:29 FTestFileChooser.form:31 FTestMenuButton.form:40 FWiki.form:20
msgid "Menu2"
msgstr ""

#: FMain.form:33 FTestFileChooser.form:36 FTestMenuButton.form:44 FWiki.form:24
msgid "Menu3"
msgstr ""

#: FMain.form:68
msgid "Élément 1"
msgstr ""

#: FMain.form:68
msgid "Élément 2"
msgstr ""

#: FMain.form:68
msgid "Élément 3"
msgstr ""

#: FMain.form:68
msgid "Élément 4"
msgstr ""

#: FMain.form:74
msgid "ComboBox2"
msgstr ""

#: FMain.form:80 FTestBalloon.form:12 FTestFileChooser.form:86
#: FTestSwitchButton.form:22
msgid "TextBox1"
msgstr ""

#: FMain.form:90 Form2.form:121
msgid "MenuButton1"
msgstr ""

#: FMessage.form:39
msgid "Do not display this message again"
msgstr "No tornis a mostrar aquest missatge"

#: FSidePanel.class:1139
msgid "Hidden"
msgstr ""

#: FSidePanel.class:1143
msgid "Transparent"
msgstr ""

#: FSpinBar.form:24
msgid "Test"
msgstr ""

#: FTestBalloon.form:17
msgid "Ceci est une bulle d'aide"
msgstr ""

#: FTestColorChooser.form:20
msgid "Resizable"
msgstr ""

#: FTestCompletion.form:28
msgid "Button2"
msgstr ""

#: FTestDateChooser.form:33
msgid "Enable"
msgstr ""

#: FTestExpander.form:16
msgid "Expander"
msgstr ""

#: FTestFileChooser.form:28 FTestMenuButton.form:36
msgid "Menu1"
msgstr ""

#: FTestFileChooser.form:41 FTestMenuButton.form:70
msgid "Menu7"
msgstr ""

#: FTestFileChooser.form:49 FTestMenuButton.form:53 FWiki.form:28
msgid "Menu4"
msgstr ""

#: FTestFileChooser.form:54 FTestMenuButton.form:57 FWiki.form:32
msgid "Menu5"
msgstr ""

#: FTestFileChooser.form:76
msgid "Balloon"
msgstr ""

#: FTestFileChooser.form:81 FTestSwitchButton.form:38
msgid "Label1"
msgstr ""

#: FTestMenuButton.form:32
msgid "Project"
msgstr ""

#: FTestMenuButton.form:49
msgid "View"
msgstr ""

#: FTestMenuButton.form:61
msgid "Menu6"
msgstr ""

#: FTestMenuButton.form:66
msgid "Tools"
msgstr ""

#: FTestMenuButton.form:74
msgid "Menu8"
msgstr ""

#: FTestMenuButton.form:78
msgid "Menu9"
msgstr ""

#: FTestMenuButton.form:81
msgid "Menu10"
msgstr ""

#: FTestMenuButton.form:85
msgid "Menu11"
msgstr ""

#: FTestMenuButton.form:124
msgid "Menu button"
msgstr ""

#: FTestSwitchButton.form:45
msgid "Label2"
msgstr ""

#: FTestTabPanel.form:41
msgid "Text"
msgstr ""

#: FTestTabPanel.form:54
msgid "Border"
msgstr ""

#: FTestTabPanel.form:59
msgid "Orientation"
msgstr ""

#: FTestToolPanel.form:17
msgid "Toolbar 1"
msgstr ""

#: FTestToolPanel.form:19
msgid "Toolbar 2"
msgstr ""

#: FTestToolPanel.form:21
msgid "Toolbar 3"
msgstr ""

#: FTestToolPanel.form:23
msgid "Toolbar 4"
msgstr ""

#: FTestValueBox.form:15
msgid "Hello world!"
msgstr ""

#: FTestWizard.form:20
msgid "Étape n°1"
msgstr ""

#: FTestWizard.form:27
msgid "Ceci est une longue étape"
msgstr ""

#: FTestWizard.form:33
msgid "Étape n°3"
msgstr ""

#: FTestWizard.form:35
msgid "Étape n°4"
msgstr ""

#: FWizard.class:88
msgid "&Next"
msgstr "&Següent"

#: FWizard.form:58 MessageView.class:50
msgid "Next"
msgstr "Següent"

#: FWizard.form:64
msgid "Previous"
msgstr "Previ"

#: FileView.class:156
msgid "No file in this folder."
msgstr ""

#: FileView.class:1139
msgid "Cannot rename file."
msgstr ""

#: Form2.form:126
msgid "ButtonBox2"
msgstr ""

#: Form3.form:25
msgid "Raise"
msgstr ""

#: Help.module:71
msgid "A file or directory name cannot be void."
msgstr ""

#: Help.module:72
msgid "The '/' character is forbidden inside file or directory names."
msgstr "El caràcter '/' no està permès dins el nom de fitxers o directoris."

#: Main.module:52
msgid "&1 KiB"
msgstr ""

#: Main.module:59
msgid "&1 MiB"
msgstr ""

#: Main.module:66
msgid "&1 GiB"
msgstr ""

#: Wizard.class:86
msgid "Step #&1"
msgstr "Pas número &1"
